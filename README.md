# BioSocialChange

Human civilization, despite all past progress, is far from optimal. The world could be a much happier, safer, and prosperous place. Yet, changes are difficult, and frought with dangers. Not least among the dangers is the danger of unintended consequences from attempts at reform.

How much time, effort, and resources have been wasted on causes that are ultimately useless or counter-productive?

BioSocialChange is a project to produce an evidence-based compendium of reforms that are free of ideological rigidity, wishful thinking, and sentimentalism. 

The starting point for such an endeavor must be from the perspective of human beings as biological creatures. Our behaviors are not those of rational agents out of a textbook, nor are we inscrutable entities whose behavior is without discenable causes. Humans are driven by incentives, but are also predictably irrational. We are subject to the forces of physical reality, and our minds are a literal part of that physical reality, however badly we may wish to think of ourselves as exempt.

A primary source of inspiration is the work of the early 20th century movement of predominantly female engineers under the euthenics movement, which sought to provide an intellectual counter-weight to the dominant eugenics movment of the time. They were also biodeterminists to a large degree, but instead argued that an approach that emphasized nurture over nature could provide results safer, cheaper, faster, and more reliably. While the use of voluntary genetic therapies is something we are hopeful of, the dark history of the eugenics movement and the powerful arguments in favor of an environmental focus must always be at the front of our minds. 

Of course, even without any focus on biology, a purely environment-focused reform agenda could easily steer into the direction of totalitarianism, as plenty of 20th century history has shown.

As such, this list of reforms will keep human freedom and sovereignty as a primary objective. This project is a liberal project, in the classical sense. Yet, a dose of conservatism is necessary to restrain our impulses to construct grand designs that may ignore unintended consequences or steamroll over human agency.

Reforms will be weighted according to whether they are important, neglected, and tractable to provide guidance to those who seek to roll up their sleeves and get to work.