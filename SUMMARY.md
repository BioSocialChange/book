# Summary

- [Introduction](README.md)

---

- [Foundational Sanity](./01-foundational-sanity.md)
- [Fighting Cognitive Impairment](./02-cognitive-impairment.md)
- [Improving Urban Space](./03-urban-space.md)
- [Healthcare](./04-healthcare.md)
- [Micronutrients and Supplements](./05-micronutrients.md)
- [Agriculture](./06-agriculture.md)
- [Animal Welfare](./07-animal-welfare.md)
- [Immigration](./08-immigration.md)
- [Fertility](./09-fertility.md)
- [Labor Law](./10-labor-law.md)
- [Social Cohesion](./11-social-cohesion.md)
- [Lenghtening Time Horizons](./12-long-time-horizons.md)
