# Foundational Sanity

- Tax the rent of land at 100%, but eliminate taxes on improvements, income, and general sales.

  - Whenever this results in surplus revenue, it should be returned as an equal dividend to all citizens.

- Establish a single-payer healthcare system with transparent pricing, copays, high deductibles, and a yearly government-provided HSA allotment equal to half the deductible, which can rollover into an IRA or 529 account if unspent.

- Switch away from expensive traditional general education, and replace much of it with supervised play, field trips, and self-direct learning.
  - Keep rote learning for the basics of elementary school (reading, writing, and arithmetic) and later for PE, health, and civics.
  - Redirect savings to increase spending on libraries, museums, gifted programs, research institutes, and trade schools

- Instead of defaulting to liberal arts education, offer and encourage more opportunities for apprenticeship, trade school, entrepreneurship, and national service.