# Lengthening Time Horizons

Human beings have variable ability to delay gratification. Everyone has their own level of time preference. Our ability to think and plan for things in the far future is limited. Too few people save for retirement. Many are fully addicted to maladaptive behaviors that destroy their future, all for short term pleasures.

Improving our ability to think and plan over long timescales, and our ability to delay gratification, is important for achieving maximum prosperity at the civilizational level.

Institutions often encourage short time horizons. The current stock market prioritizes quarterly earnings above long term plans. The news cycle is very rapid, because the business model of media companies prioritizes sensationalism and viral content.

- Sapir-Whorf hypothesis
    - Speakers of futureless languages have a higher savings rate.

- Efforts such as the Long Term Stock Exchange hope to reorient companies to longer time horizons.