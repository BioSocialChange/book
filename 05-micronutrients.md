# Micronutrients and Supplements

- Optimize micronutrients in the water supply
  - Lithium
  - Silica

- Optimize selenium levels in the soil

- Fortify vegan products with missing carninutrients
  - Vitamin B12
  - Creatine
  - Taurine
  - Carnitine
  - Carnosine

- Optimize infant formula
  - Enfamil Enspire and Similac Pro Advance should become the baseline level of quality

- Encourage yearly bloodwork that tracks micronutrient biomarkers